/*
	Client-Server Archictecture
	- a computing model wherein a server hosts, delivers and manages resources that a client consumes.

	Client is an application which creates requests from a server. A client will trigger an action and response from the server.

	Server is able to host and deliver resources requested by the client. A server can ansert to multiple clients.

	Node.js
	- created because JavaScript was originally created and conceptialized to be used for front-end development. Node.js is an open source environment which allows to create backend applications using JS.

	Why is Node.s popular?
		Performance - Node.js is one of the most performing environment for creating backend application for JS

		NPM- Node Package Manager - it is one of the largest registry for packages. Packages are methods, functions and code that greatly helps or adds with applications.

		Familiarity - JS is one of the most popular programming languages and Node.js uses JS  as its primary languages
*/
let http = require("http");
// we use the require directive to load Node.js modules
// a module is a software component or part of a program that contains one or more routines
// HTTP is a protocol that allows the fetching of resources such as HTML documents

http.createServer(function (require, response){
// createServer is method na kasama sa http module
	response.writeHead(200, {'Content-Type': 'text/plain'});
/*Use the writeHead() method to:
	set a status code for the response - 200 means OK
	set the content-type of the response as a plain text message
*/
	response.end('Hello World');
	// This sends the response with the text content 'Hello World'

}).listen(4000)
// A port is a virtual point where the network connections start and end
// The server will be assigned to port 4000 via the listen(4000) method where the server will listen to any requeset that are sent to it eventually communication with our server

console.log('Server Running at localhost:4000');
// When server is running, console will print the message